const express = require('express');
const modelProvedor = require('../../Models/refaccionaria/modelProvedor');

let app = express();

//REGISTRAR NUEVO PERSONAL
app.post('/provedor/nuevo',(req, res) =>{
    let body = req.body;
    console.log(body);

    let newSchemaProvedor = new modelProvedor({
        nombreProvedor : body.nombreProvedor,
        apellidoPProvedor : body.apellidoPProvedor,
        apellidoMProvedor : body.apellidoMProvedor,
        edadProvedor : body.edadProvedor,
        telefonoProvedor : body.telefonoProvedor,
    });
    newSchemaProvedor
    .save()
    .then(
        (data) => {
            return res.status(200)
                .json({
                    ok: true,
                    message:'Datos guardados',
                    data
                });
        }
        )

        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message:'no se guardaron los Datos',
                        err
                    });
            }
        )
});

//BUSCAR PROVEDOR
app.get('/obtener/provedor', async (req,res)=>{
    let id= req.params.id;
    const respuesta = await modelProvedor.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//ACTUALIZAR PROVEDOR
app.put('/update/provedor/:id',async (req,res)=>{
    let id =req.params.id;
    //como actualizar GENERAR CONSTANTE
    const campos =req.body;

    const respuesta = await modelProvedor.findByIdAndUpdate(id,campos,{new:true});//ya actualizado nos va a regresar el documento original
    res.status(202).json({
        ok:true, //condicion
        msj:"DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

//ELIMINAR PROVEDOR
app.delete('/delete/provedor/:id',async(req,res)=>{
    let id =req.params.id;//es un parametro
    const respuesta = await modelProvedor.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;