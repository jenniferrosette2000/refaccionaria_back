const express = require('express');
const modelProductoNuevo = require('../../Models/refaccionaria/modelNuevoProducto');

let app = express();

//REGISTRAR NUEVO USUARIO
app.post('/producto/nuevo',(req, res) =>{
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelProductoNuevo({
        nombreProducto : body.nombreProducto,
        marcaProducto : body.marcaProducto,
        presentacionProducto : body.presentacionProducto,
        contenidoPriducto : body.contenidoPriducto,
        CostoPriducto : body.CostoPriducto,
        proveedorProducto : body.proveedorProducto,
        cantidadIngresa: body.cantidadIngresa,
        statusProducto: body.statusProducto,
        descripcionProducto: body.descripcionProducto,
    });
    newSchemaProducto
    .save()
    .then(
        (data) => {
            return res.status(200)
                .json({
                    ok: true,
                    message:'Datos guardados',
                    data
                });
        }
        )

        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message:'no se guardaron los Datos',
                        err
                    });
            }
        )
});

//BUSCAR USUARIO
app.get('/obtener/producto', async (req,res)=>{
    let id= req.params.id;
    const respuesta = await modelProductoNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });

});


//ACTUALIZAR PRODUCTO
app.put('/update/producto/:id',async (req,res)=>{
    let id =req.params.id;
    //como actualizar GENERAR CONSTANTE
    const campos =req.body;

    const respuesta = await modelProductoNuevo.findByIdAndUpdate(id,campos,{new:true});//ya actualizado nos va a regresar el documento original
    res.status(202).json({
        ok:true, //condicion
        msj:"DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

//ELIMINAR PRODUCTO
app.delete('/delete/producto/:id',async(req,res)=>{
    let id =req.params.id;//es un parametro
    const respuesta = await modelProductoNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;