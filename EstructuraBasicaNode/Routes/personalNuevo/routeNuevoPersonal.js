const express = require('express');
const modelPersonal = require('../../Models/refaccionaria/modelPersonal');

let app = express();

//REGISTRAR NUEVO PERSONAL
app.post('/personal/nuevo',(req, res) =>{
    let body = req.body;
    console.log(body);

    let newSchemaPersonal = new modelPersonal({
        nombrePersonal : body.nombrePersonal,
        apellidoPPersonal : body.apellidoPPersonal,
        apellidoMPersonal : body.apellidoMPersonal,
        edadPersonal : body.edadPersonal,
        telefonoPersonal : body.telefonoPersonal,
    });
    newSchemaPersonal
    .save()
    .then(
        (data) => {
            return res.status(200)
                .json({
                    ok: true,
                    message:'Datos guardados',
                    data
                });
        }
        )

        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message:'no se guardaron los Datos',
                        err
                    });
            }
        )

});

//BUSCAR USUARIO
app.get('/obtener/personal', async (req,res)=>{
    let id= req.params.id;
    const respuesta = await modelPersonal.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//ACTUALIZAR PERSONAL
app.put('/update/personal/:id',async (req,res)=>{
    let id =req.params.id;
    //como actualizar GENERAR CONSTANTE
    const campos =req.body;

    const respuesta = await modelPersonal.findByIdAndUpdate(id,campos,{new:true});//ya actualizado nos va a regresar el documento original
    res.status(202).json({
        ok:true, //condicion
        msj:"DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

//ELIMINAR PERSONAL
app.delete('/delete/personal/:id',async(req,res)=>{
    let id =req.params.id;//es un parametro
    const respuesta = await modelPersonal.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });

});

module.exports = app;