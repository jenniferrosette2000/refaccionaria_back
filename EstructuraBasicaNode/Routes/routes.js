//? va a contener los metodos GET, DELETE, UPDATE, POST, PUT
const express = require ('express');
const app = express();


app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./personalNuevo/routeNuevoPersonal'));
app.use(require('./provedorNuevo/routeNuevoProvedor'));
app.use(require('./sucursalNuevo/routeNuevoSucursal'));

module.exports = app;