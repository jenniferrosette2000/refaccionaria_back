const express = require('express');
const modelSucursal = require('../../Models/refaccionaria/modelSucursal');

let app = express();

//REGISTRAR NUEVO PERSONAL
app.post('/sucursal/nuevo',(req, res) =>{
    let body = req.body;
    console.log(body);

    let newSchemaSucursal = new modelSucursal({
        nombreSucursal : body.nombreSucursal,
        ubicacionSucursal : body.ubicacionSucursal,
        encargadoSucursal : body.encargadoSucursal,
        horarioSucursal : body.horarioSucursal,
        numeroSucursal : body.numeroSucursal,
    });
    newSchemaSucursal
    .save()
    .then(
        (data) => {
            return res.status(200)
                .json({
                    ok: true,
                    message:'Datos guardados',
                    data
                });
        }
        )

        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message:'no se guardaron los Datos',
                        err
                    });
            }
        )
});

//BUSCAR SUCURSAL
app.get('/obtener/sucursal', async (req,res)=>{
    let id= req.params.id;
    const respuesta = await modelSucursal.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//ACTUALIZAR SUCURSAL
app.put('/update/sucursal/:id',async (req,res)=>{
    let id =req.params.id;
    //como actualizar GENERAR CONSTANTE
    const campos =req.body;

    const respuesta = await modelSucursal.findByIdAndUpdate(id,campos,{new:true});//ya actualizado nos va a regresar el documento original
    res.status(202).json({
        ok:true, //condicion
        msj:"DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});


//ELIMINAR SUCURSAL
app.delete('/delete/sucursal/:id',async(req,res)=>{
    let id =req.params.id;//es un parametro
    const respuesta = await modelSucursal.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;