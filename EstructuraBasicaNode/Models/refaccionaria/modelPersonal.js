const mongoose = require('mongoose');
let Schema = mongoose.Schema;

//MODELO DEL DOCUMENTO
let nuevoPersonal = new Schema({
    nombrePersonal : {type: String},
    apellidoPPersonal : {type: String},
    apellidoMPersonal : {type: String},
    edadPersonal : {type: String},
    telefonoPersonal : {type: String},
});

module.exports = mongoose.model('nuevoPersonal', nuevoPersonal);