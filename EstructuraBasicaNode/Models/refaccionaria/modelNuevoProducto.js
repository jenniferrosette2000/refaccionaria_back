const mongoose = require('mongoose');
let Schema = mongoose.Schema;

//MODELO DEL DOCUMENTO
let nuevoProductoRefaccionaria = new Schema({
    nombreProducto : {type: String},
    marcaProducto : {type: String},
    presentacionProducto : {type: String},
    contenidoPriducto : {type: String},
    CostoPriducto : {type: Number},
    proveedorProducto : {type: String},
    cantidadIngresa: { type: Number},
    statusProducto: { type: Boolean},
    descripcionProducto: { type: String},
});

module.exports = mongoose.model('nuevoProducto', nuevoProductoRefaccionaria);