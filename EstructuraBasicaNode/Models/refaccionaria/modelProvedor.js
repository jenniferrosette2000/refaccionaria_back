const mongoose = require('mongoose');
let Schema = mongoose.Schema;

//MODELO DEL DOCUMENTO
let nuevoProvedor = new Schema({
    nombreProvedor : {type: String},
    apellidoPProvedor : {type: String},
    apellidoMProvedor : {type: String},
    edadProvedor : {type: String},
    telefonoProvedor : {type: String},
});

module.exports = mongoose.model('nuevoProvedor', nuevoProvedor);