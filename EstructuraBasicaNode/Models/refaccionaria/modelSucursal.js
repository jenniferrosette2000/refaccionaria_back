const mongoose = require('mongoose');
let Schema = mongoose.Schema;

//MODELO DEL DOCUMENTO
let nuevoSucursal = new Schema({
    nombreSucursal : {type: String},
    ubicacionSucursal : {type: String},
    encargadoSucursal : {type: String},
    horarioSucursal : {type: String},
    numeroSucursal : {type: String},
});

module.exports = mongoose.model('nuevoSucursal', nuevoSucursal);