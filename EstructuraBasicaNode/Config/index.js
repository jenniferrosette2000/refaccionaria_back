//? Guardara las configuraciones, las clases y constantes de Clusters en la nube, entre otras cosas.
console.log('nodemon ejecutandose CORRECTAENTE');

const mongoose = require('mongoose');
const app = require('../Server/index');
const port = 3900;

//GENERAR PROMESA GLOBAL
mongoose.Promise = global.Promise ; //cumplir ciertas promesas

//HACER LA CONEXION A LA BD
//PONER LA DIRECCION EN VEZ DE LOCALHOST
mongoose.connect('mongodb://127.0.0.1:27017/refaccionaria',{useNewUrlParser: true})
    .then(() => {
        console.log('BASE DE DATOS CORRIENDO');

    //ESCUCHA EL PUERTO SERVER
    app.listen(port, () => {
        console.log(`server corriendo en el puerto: ${port} `);
    });
});
