//Contiene la configuracion del servidor
'use strict'

//llamar las librerias de body parse y express
const bodyParser = require ('body-parser');
const express = require ('express');

//iniciamos EXPRESS
const app = express ();

//ACTIVAR LOS CORS
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept")
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

//ACTIVAR LOS MIDLEWARES DE EXPRESS (LOS CORS PERMITEN HACER EL INTERCAMBIO DE INF ENTRE SERVIDORES)
app.use(bodyParser.urlencoded({ extended : false}));
app.use(bodyParser.json());
app.use(require('../Routes/routes'));

module.exports = app; //ES NECESARIO EXPORTAR LOS MODULOS